
# Script to process HTML to find all references and links
Script will process through given HTML file(s) or URL(s) to find all elements under tags: 

>`<a>`, `<img>`, `<script>`,`<link>`

Then it collects  `href`,  `src`  and  `link`  attributes to display them in JSON format.
## To run
In the command line 
>`cd html-links-processor/`<br/>
>`php src-parser-to-json.php arg1 arg2 ...`