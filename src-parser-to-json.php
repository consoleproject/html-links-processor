<?php
	define("REQUEST_CONTEXT", stream_context_create(
			[
				"http" => 
					[
						"header" => "User-Agent: Mozilla/5.0 (Android 12; Mobile; rv:68.0) Gecko/68.0 Firefox/98.0"
					]
			]
		)
	);
	define("TAGS", ["a", "img", "script", "link"]);
	define("ATTR", ["href", "link", "src"]);
	define("ARGV", $argv);
	
	function flow() {
		$dom_document = new DOMDocument();
		$argv = ARGV; // passed arguments
		$number_of_args = count($argv); // besides given urls/files, this php file is also counted
		$html_files = []; // files/urls
		
		if ($number_of_args > 1) {
			for ($arg_key = 1; $arg_key <= $number_of_args - 1; $arg_key++) {
				array_push($html_files, [
	                "html_url" => $argv[$arg_key],
	                "html_content" => file_get_contents($argv[$arg_key], false, REQUEST_CONTEXT)
	            ]);
			}

			$json_array = [];
			foreach ($html_files as $html_file_key => $html_file) {
				// in order to avoid long list of warnings caused by loadHTML() I have
				// used proposed solution from stackoverflow.com, below is the link
				// https://stackoverflow.com/questions/9149180/domdocumentloadhtml-error
				libxml_use_internal_errors(true);
				$dom_document->loadHTML($html_file["html_content"]);
				libxml_use_internal_errors(false);
				
				foreach (TAGS as $tag_key => $tag) {
					$tags_object = $dom_document->getElementsByTagName($tag);
					$json_array[$html_file["html_url"]][$tag] = tagAttributesJson($tag, $tags_object);
				}
			}

			$pretty_json = json_encode($json_array, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES);
			return print_r($pretty_json . "\n");
		} 
		echo "Provide argument(s)\n";
	}

	function tagAttributesJson($tag_name, $all_tags_obj) {
		// get href, src, link attributes of all elements under $tag_name
		$tag_attributes_array = [];
		foreach ($all_tags_obj as $tag_key => $single_tag) {
			$tag_attributes = $single_tag->attributes;
			foreach ($tag_attributes as $attribute) {
				if (in_array($attribute->name, ATTR)) {
					if ($attribute->value !== "") {
						array_push($tag_attributes_array, $attribute->value);
					}
				}
			}
		}
		return $tag_attributes_array;
	}

	flow();
?>